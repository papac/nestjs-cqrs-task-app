import { Status } from 'src/task/task.entity';

export class UpdateTaskCommand {
  constructor(
    public readonly id: number,
    public readonly title: string,
    public readonly content: string,
    public readonly status?: Status,
  ) {}
}
