export class DeleteTaskCommand {
  constructor(public readonly taskId: number) {}
}
