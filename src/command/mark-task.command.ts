import { Status } from 'src/task/task.entity';

export class MarkTaskCommand {
  constructor(public id: number, public status: Status) {}
}
