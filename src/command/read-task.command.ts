export class ReadTaskCommand {
  constructor(public readonly taskId?: number) {}
}
