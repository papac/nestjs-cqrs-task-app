import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TYPEORM_DATABASE_CONFIGURATION } from './config/database.config';
import { TaskModule } from './task/task.module';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forRoot(TYPEORM_DATABASE_CONFIGURATION),
    TaskModule,
  ],
})
export class AppModule {}
