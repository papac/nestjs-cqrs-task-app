import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const connection = {
  database: 'task_database',
  driver: 'postgres',
  host: 'localhost',
  password: 'password',
  user: 'task_user',
  port: '5433',
};

const redisConnection: {
  url?: string;
  host?: string;
  port?: number;
} = {
  host: 'localhost',
  port: 6379,
};

export const REDIS_CONFIGURATION = redisConnection;
export const DATABASE_PORT = parseInt(connection.port, 10);
export const DATABASE_HOSTNAME = connection.host;
export const DATABASE_USERNAME = connection.user;
export const DATABASE_PASSWORD = connection.password;
export const DATABASE_NAME = connection.database;
export const TYPEORM_DATABASE_CONFIGURATION: TypeOrmModuleOptions = {
  type: 'postgres',
  host: DATABASE_HOSTNAME,
  port: DATABASE_PORT,
  username: DATABASE_USERNAME,
  password: DATABASE_PASSWORD,
  database: DATABASE_NAME,
  entities: [__dirname + '/../**/*.entity.{ts,js}'],
  subscribers: [],
  migrationsTableName: 'migrations',
  cli: {
    migrationsDir: 'migration',
    subscribersDir: 'subscriber',
  },
  synchronize: true,
};
