import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { DeleteTaskCommand } from 'src/command/delete-task.command';
import { CreateTaskCommand } from 'src/command/create-task.command';
import { ReadTaskCommand } from 'src/command/read-task.command';
import { UpdateTaskCommand } from 'src/command/update-task.command';
import { MarkTaskCommand } from 'src/command/mark-task.command';
import { Status } from './task.entity';

@Injectable()
export class TaskService {
  constructor(private commandBus: CommandBus) {}

  async getTasks() {
    return this.commandBus.execute(new ReadTaskCommand());
  }

  async getTask(taskId: number) {
    return this.commandBus.execute(new ReadTaskCommand(taskId));
  }

  async createTask(taskDto) {
    return this.commandBus.execute(
      new CreateTaskCommand(taskDto.title, taskDto.content),
    );
  }

  async updateTask(taskId: number, taskDto) {
    return this.commandBus.execute(
      new UpdateTaskCommand(
        taskId,
        taskDto.title,
        taskDto.content,
        taskDto.status,
      ),
    );
  }

  async markTask(taskId: number, status: Status) {
    return this.commandBus.execute(new MarkTaskCommand(taskId, status));
  }

  async removeTask(taskId) {
    return this.commandBus.execute(new DeleteTaskCommand(taskId));
  }
}
