import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreateTaskCommandHandler } from 'src/handler/create-task.handler';
import { DeleteTaskCommandHandler } from 'src/handler/delete-task.handler';
import { MarkTaskCommandHandler } from 'src/handler/mark-task.handle';
import { ReadTaskCommandHandler } from 'src/handler/read-task.handler';
import { UpdateTaskCommandHandler } from 'src/handler/update-task.handler';
import { TaskController } from './task.controller';
import { TaskEntity } from './task.entity';
import { TaskService } from './task.service';

export const CommandHandlers = [
  CreateTaskCommandHandler,
  DeleteTaskCommandHandler,
  UpdateTaskCommandHandler,
  ReadTaskCommandHandler,
  MarkTaskCommandHandler,
];

@Module({
  imports: [CqrsModule, TypeOrmModule.forFeature([TaskEntity])],
  controllers: [TaskController],
  providers: [TaskService, ...CommandHandlers],
})
export class TaskModule {}
