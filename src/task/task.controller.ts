import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
} from '@nestjs/swagger';
import { Status } from './task.entity';
import { TaskService } from './task.service';
import CreateTaskSchema from '../swagger/create.schema';
import UpdateTaskSchema from '../swagger/update.schema';
import TaskSchema from '../swagger/task.schema';
import generateStatus from '../swagger/status.schema';

@Controller('tasks')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get()
  @ApiOkResponse({
    description: 'Get task list successful',
    schema: {
      properties: {
        ...generateStatus('TASK_LIST'),
        data: {
          type: 'array',
          items: { type: 'object', properties: TaskSchema },
        },
      },
    },
  })
  async getTasks() {
    const tasks = await this.taskService.getTasks();
    return {
      status: {
        code: 'OK',
        message: 'Ok',
      },
      data: tasks,
    };
  }

  @Get(':taskId')
  @ApiParam({
    description: 'Add task id to params',
    name: 'taskId',
    schema: {
      properties: {
        taskId: { type: 'number' },
      },
    },
  })
  @ApiOkResponse({
    description: 'Get task successful',
    schema: {
      properties: {
        ...generateStatus('TASK_ITEM'),
        data: { type: 'object', properties: TaskSchema },
      },
    },
  })
  @ApiNotFoundResponse({
    description: 'Task not found',
    schema: {
      properties: generateStatus('TASK_NOT_FOUND'),
    },
  })
  async getTask(@Param('taskId') taskId: number) {
    console.log(taskId);
    const task = await this.taskService.getTask(taskId);
    return {
      status: {
        code: 'TASK_ITEM',
        message: 'Get task successful',
      },
      data: task,
    };
  }

  @Post()
  @ApiCreatedResponse({
    description: 'Task created successful',
    schema: {
      properties: {
        ...generateStatus('TASK_CREATED'),
        data: { type: 'object', properties: TaskSchema },
      },
    },
  })
  @ApiBody({
    schema: {
      properties: CreateTaskSchema,
    },
  })
  async createTask(@Body() payload: { title: string; content: string }) {
    const task = await this.taskService.createTask(payload);
    return {
      status: {
        code: 'TASK_CREATED',
        message: 'Task created successful',
      },
      data: task,
    };
  }

  @Put(':taskId')
  @ApiParam({
    description: 'Add task id to params',
    name: 'taskId',
    schema: {
      properties: {
        taskId: { type: 'number' },
      },
    },
  })
  @ApiOkResponse({
    description: 'Task updated successful',
    schema: {
      properties: {
        ...generateStatus('TASK_UPDATED'),
        data: { type: 'object', properties: TaskSchema },
      },
    },
  })
  @ApiBody({
    schema: {
      properties: UpdateTaskSchema,
    },
  })
  @ApiNotFoundResponse({
    description: 'Task not found',
    schema: {
      properties: generateStatus('TASK_NOT_FOUND'),
    },
  })
  async updateTask(
    @Body() payload: { title: string; content: string; status?: Status },
    @Param('taskId') taskId: number,
  ) {
    const task = await this.taskService.updateTask(taskId, payload);
    return {
      status: {
        code: 'TASK_UPDATED',
        message: 'Task updated successful',
      },
      data: task,
    };
  }

  @Patch(':taskId')
  @ApiParam({
    description: 'Add task id to params',
    name: 'taskId',
    schema: {
      properties: {
        taskId: { type: 'number' },
      },
    },
  })
  @ApiQuery({
    description: 'Change task status',
    name: 'status',
    schema: {
      properties: {
        status: { type: 'string' },
      },
    },
  })
  @ApiOkResponse({
    description: 'Task status updated successful',
    schema: {
      properties: generateStatus('TASK_MARKED'),
    },
  })
  async markTask(
    @Param('taskId') taskId: number,
    @Query('status') status: Status,
  ) {
    console.log(status);
    await this.taskService.markTask(taskId, status);
    return {
      status: {
        code: 'TASK_MARKED',
        message: 'Task status updated successful',
      },
    };
  }

  @Delete(':taskId')
  @ApiQuery({
    description: 'Delete task',
    schema: {
      properties: {
        taskId: { type: 'number' },
      },
    },
  })
  @ApiOkResponse({
    description: 'Task deleted successful',
    schema: {
      properties: {
        ...generateStatus('TASK_DELETED|TASK_NOT_FOUND'),
        data: { type: 'object', properties: TaskSchema },
      },
    },
  })
  async deleteTask(@Param('taskId') taskId: number) {
    try {
      const task = await this.taskService.removeTask(taskId);
      return {
        status: {
          code: 'TASK_DELETED',
          message: 'Task deleted successful',
        },
        data: task,
      };
    } catch (e) {
      return {
        status: {
          code: 'TASK_NOT_FOUND',
          message: 'Task is unreachable',
        },
      };
    }
  }
}
