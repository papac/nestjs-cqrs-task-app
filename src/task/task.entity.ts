import { ApiProperty } from '@nestjs/swagger';
import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum Status {
  'pending',
  'completed',
}

@Entity({
  name: 'tasks',
})
export class TaskEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    description: 'The task title',
  })
  @Column({
    type: 'varchar',
    length: 255,
  })
  title: string;

  @ApiProperty({
    description: 'The task status',
  })
  @Column({
    type: 'varchar',
    enum: ['pending', 'completed'],
    default: 'pending',
  })
  status: number;

  @ApiProperty({
    description: 'The task content',
  })
  @Column({
    type: 'text',
  })
  content: string;

  @ApiProperty({
    description: 'The task creation date',
  })
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty({
    description: 'The task updated date',
  })
  @UpdateDateColumn()
  updatedAt: Date;
}
