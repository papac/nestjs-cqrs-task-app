import { Test, TestingModule } from '@nestjs/testing';
import { TaskEntity } from './task.entity';
import { TaskService } from './task.service';

describe('TaskService', () => {
  let service: TaskService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaskService],
    }).compile();

    service = module.get<TaskService>(TaskService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be create a new task', async () => {
    const task = await service.createTask({
      title: 'Todo',
      content: 'Deploy a new app',
    });
    expect(task).toBeInstanceOf(TaskEntity);
    expect(task.title).toEqual('Todo');
    expect(task.content).toEqual('Deploy a new app');
  });

  it('should be get list of tasks', async () => {
    const tasks = await service.getTasks();
    expect(tasks).toContain({ title: 'Todo', content: 'Deplot a new app' });
  });

  it('should be get one of tasks', async () => {
    const task = await service.getTask(1);
    expect(task).toMatchObject({
      id: 1,
      title: 'Todo',
      content: 'Deploy a new app',
    });
  });

  it('should be not get the task', async () => {
    const task = await service.getTask(2);
    expect(task).toBeUndefined();
  });

  it('should be delete task', async () => {
    const task = await service.removeTask(1);
    expect(task).toBe(true);
  });

  it('should be not find deleted task', async () => {
    const task = await service.removeTask(1);
    expect(task).toBeUndefined();
  });

  it('should be update task', async () => {
    const task = await service.getTask(1);
    expect(task).toBeDefined();
    const updatedTask = await service.updateTask(task.id, {
      title: 'Todo Updated',
      content: 'Deploy app soon',
    });
    expect(updatedTask.id).toEqual(task.id);
    expect(updatedTask.title).not.toEqual(task.title);
    expect(updatedTask.content).not.toEqual(task.content);
  });
});
