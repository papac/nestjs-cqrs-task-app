export default {
  status: {
    type: 'number',
    default: 'optional',
  },
  title: {
    type: 'string',
  },
  content: {
    type: 'string',
  },
};
