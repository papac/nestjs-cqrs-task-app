export default {
  id: {
    type: 'number',
  },
  title: {
    type: 'string',
  },
  content: {
    type: 'string',
  },
  createdAt: {
    type: 'string',
  },
  updatedAt: {
    type: 'string',
  },
  status: {
    type: 'string',
    default: 'pending',
  },
};
