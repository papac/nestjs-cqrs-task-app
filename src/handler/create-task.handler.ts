import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTaskCommand } from 'src/command/create-task.command';
import { TaskEntity } from 'src/task/task.entity';
import { Repository } from 'typeorm';

@CommandHandler(CreateTaskCommand)
export class CreateTaskCommandHandler
  implements ICommandHandler<CreateTaskCommand>
{
  constructor(
    @InjectRepository(TaskEntity)
    private repository: Repository<TaskEntity>,
  ) {}

  async execute(_command: CreateTaskCommand): Promise<TaskEntity> {
    const { title, content } = _command;
    const task = new TaskEntity();
    task.title = title;
    task.content = content;
    return await this.repository.save(task);
  }
}
