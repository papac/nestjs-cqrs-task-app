import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { MarkTaskCommand } from 'src/command/mark-task.command';
import { TaskEntity } from 'src/task/task.entity';
import { Repository } from 'typeorm';

@CommandHandler(MarkTaskCommand)
export class MarkTaskCommandHandler
  implements ICommandHandler<MarkTaskCommand>
{
  constructor(
    @InjectRepository(TaskEntity)
    private repository: Repository<TaskEntity>,
  ) {}

  async execute(_command: MarkTaskCommand): Promise<TaskEntity> {
    const { status, id } = _command;
    const task = await this.repository.findOne(id);
    task.status = status;
    task.updatedAt = new Date();
    return await this.repository.save(task);
  }
}
