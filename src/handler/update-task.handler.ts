import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateTaskCommand } from 'src/command/update-task.command';
import { TaskEntity } from 'src/task/task.entity';
import { Repository } from 'typeorm';

@CommandHandler(UpdateTaskCommand)
export class UpdateTaskCommandHandler
  implements ICommandHandler<UpdateTaskCommand>
{
  constructor(
    @InjectRepository(TaskEntity)
    private repository: Repository<TaskEntity>,
  ) {}

  async execute(_command: UpdateTaskCommand): Promise<TaskEntity> {
    const { title, content, status, id } = _command;
    const task = await this.repository.findOne(id);
    if (status) {
      task.status = status;
    }
    task.title = title;
    task.content = content;
    task.updatedAt = new Date();
    return await this.repository.save(task);
  }
}
