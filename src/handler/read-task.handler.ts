import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ReadTaskCommand } from 'src/command/read-task.command';
import { TaskEntity } from 'src/task/task.entity';
import { Repository } from 'typeorm';

@CommandHandler(ReadTaskCommand)
export class ReadTaskCommandHandler
  implements ICommandHandler<ReadTaskCommand>
{
  constructor(
    @InjectRepository(TaskEntity)
    private repository: Repository<TaskEntity>,
  ) {}

  async execute(_command: ReadTaskCommand): Promise<TaskEntity[] | TaskEntity> {
    const { taskId } = _command;
    if (taskId) {
      return await this.repository.findOne(taskId);
    }
    return await this.repository.find({ order: { createdAt: 'DESC' } });
  }
}
