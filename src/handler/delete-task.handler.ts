import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteTaskCommand } from 'src/command/delete-task.command';
import { TaskEntity } from 'src/task/task.entity';
import { Repository } from 'typeorm';

@CommandHandler(DeleteTaskCommand)
export class DeleteTaskCommandHandler
  implements ICommandHandler<DeleteTaskCommand>
{
  constructor(
    @InjectRepository(TaskEntity) private repository: Repository<TaskEntity>,
  ) {}

  async execute(command: DeleteTaskCommand): Promise<TaskEntity> {
    const { taskId } = command;
    const task = await this.repository.findOne(taskId);
    return await task.remove();
  }
}
