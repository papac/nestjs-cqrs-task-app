export default {
  database: 'task_database',
  type: 'postgres',
  driver: 'postgres',
  host: 'localhost',
  password: 'password',
  user: 'task_user',
  port: '5432',
};
